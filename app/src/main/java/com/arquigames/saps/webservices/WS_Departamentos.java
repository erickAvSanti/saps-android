package com.arquigames.saps.webservices;

import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arquigames.saps.AppActivity;
import com.arquigames.saps.LoginActivity;
import com.arquigames.saps.zonas.DepartamentoActivity;
import com.arquigames.saps.zonas.ItemDepartamentoListActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WS_Departamentos {
    private static boolean try_reload = true;
    public static void reload(final ItemDepartamentoListActivity ctx){
        if(!try_reload)return;
        Log.e(ItemDepartamentoListActivity.TAG, "Reload WS_Departamentos");
        try_reload = false;
        //showProgress(false);
        RequestQueue queue = Volley.newRequestQueue(ctx);

        StringRequest objRequest = new StringRequest(Request.Method.POST, AppActivity.URL_DEPARTAMENTOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_reload = true;
                //showProgress(false);
                JSONObject json = null;
                try {
                    json = new JSONObject(response);
                    Boolean success= (Boolean)json.get("success");
                    if(success){
                        Log.e(ItemDepartamentoListActivity.TAG,response);
                        JSONArray data = json.getJSONArray("data");
                        ctx.fillList(data);
                    }else{
                        //TODO
                        Log.e(ItemDepartamentoListActivity.TAG, String.format("cant get info => %s", AppActivity.URL_DEPARTAMENTOS));
                    }
                } catch (JSONException e) {
                    Log.e(ItemDepartamentoListActivity.TAG, LoginActivity.getStackTrace(e));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_reload = true;
                //showProgress(false);
            }
        });
        queue.add(objRequest);
    }
    public static void reload2(final DepartamentoActivity ctx){
        if(!try_reload)return;
        Log.e(ItemDepartamentoListActivity.TAG, "Reload WS_Departamentos");
        try_reload = false;
        //showProgress(false);
        RequestQueue queue = Volley.newRequestQueue(ctx);

        StringRequest objRequest = new StringRequest(Request.Method.POST, AppActivity.URL_DEPARTAMENTOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try_reload = true;
                //showProgress(false);
                JSONObject json = null;
                try {
                    json = new JSONObject(response);
                    Boolean success= (Boolean)json.get("success");
                    if(success){
                        Log.e(ItemDepartamentoListActivity.TAG,response);
                        JSONArray data = json.getJSONArray("data");
                        ctx.fillList(data);
                    }else{
                        //TODO
                        Log.e(ItemDepartamentoListActivity.TAG, String.format("cant get info => %s", AppActivity.URL_DEPARTAMENTOS));
                    }
                } catch (JSONException e) {
                    Log.e(ItemDepartamentoListActivity.TAG, LoginActivity.getStackTrace(e));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try_reload = true;
                //showProgress(false);
            }
        });
        queue.add(objRequest);
    }
}
