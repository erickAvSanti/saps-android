
package com.arquigames.saps.fragments;

        import android.app.Dialog;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatDialogFragment;
        import android.view.LayoutInflater;

        import com.arquigames.saps.R;

public class FragmentPolizas extends AppCompatDialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    private OnFragmentInteractionListener mListener;

    public FragmentPolizas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentCotizaciones.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCotizaciones newInstance(String param1, String param2) {
        FragmentCotizaciones fragment = new FragmentCotizaciones();
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_polizas, null));
        return builder.create();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
