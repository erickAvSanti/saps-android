package com.arquigames.saps.broadcastreceivers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.arquigames.saps.AppActivity;
import com.arquigames.saps.LoginActivity;

import static android.content.Context.AUDIO_SERVICE;

public class PhoneCallReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    public static String savedNumber;  //because the passed incoming is only valid in ringing
    private static final String TAG = "PhoneCallReceiver";

    public static boolean wasRinging = false;
    private static MediaRecorder recorder;
    public static File audiofile;
    private static boolean recordstarted = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if ("android.intent.action.NEW_OUTGOING_CALL".equals(intent.getAction())) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String phoneNumber = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){//1
                state = TelephonyManager.CALL_STATE_RINGING;
                wasRinging = true;
                Log.e(TAG, "wasRinging "+wasRinging);
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){//2
                state = TelephonyManager.CALL_STATE_OFFHOOK;
                wasRinging = true;
                Log.e(TAG, "TRY RECORDING.....");
                Log.e(TAG, "wasRinging "+wasRinging);
                if(wasRinging){
                    Log.e(TAG, "TRY RECORDING START.....");
                    String out = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss", Locale.US).format(new Date());
                    File sampleDir = new File(Environment.getExternalStorageDirectory(), "/"+ AppActivity.CALL_RECORD_DIRECTORY);
                    if (!sampleDir.exists()) {
                        sampleDir.mkdirs();
                    }
                    Log.e(TAG, "abs path => "+sampleDir.getAbsolutePath());
                    String file_name = String.format("%s %s", phoneNumber, out);
                    //audiofile = File.createTempFile(file_name, ".amr", sampleDir);
                    audiofile = new File(sampleDir,file_name+".amr");

                    recorder = new MediaRecorder();
//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

                    recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                    recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                    recorder.setOutputFile(audiofile.getAbsolutePath());

                    AudioManager audioManager =
                            (AudioManager)context.getSystemService(AUDIO_SERVICE);
                    audioManager.setMode(AudioManager.MODE_IN_CALL);
                    audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
                    try {
                        recorder.prepare();
                    } catch (IllegalStateException e) {
                        Log.e(TAG, LoginActivity.getStackTrace(e));
                    } catch (IOException e) {
                        Log.e(TAG, LoginActivity.getStackTrace(e));
                    }
                    recorder.start();
                    recordstarted = true;
                }
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){//0
                state = TelephonyManager.CALL_STATE_IDLE;
                wasRinging = false;
                Log.e(TAG, "wasRinging "+wasRinging);
                if (recordstarted) {
                    recorder.stop();
                    recordstarted = false;
                    audiofile = null;
                    recorder.release();
                    recorder = null;
                }
                AudioManager audioManager =
                        (AudioManager)context.getSystemService(AUDIO_SERVICE);
                audioManager.setMode(AudioManager.MODE_NORMAL);
            }
            Log.e(TAG,"State : "+state+", phoneNumber : "+phoneNumber);
        }
    }
}