package com.arquigames.saps.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.arquigames.saps.LoginActivity;

public class GpsLocationReceiver extends BroadcastReceiver {

    public static final String TAG = "GpsLocationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle extras = intent.getExtras();

        if(extras==null){
            Log.e(TAG,"extras is null");
            return;
        }
        String action = intent.getAction();
        Location loc;
        Log.e(TAG, String.format("Action : %s", String.valueOf(action)));
        try {
            loc = (Location)extras.get(LocationManager.KEY_LOCATION_CHANGED);
            Log.e(TAG, String.valueOf(loc));
        }catch(Exception e) {
            Log.e(TAG, LoginActivity.getStackTrace(e));
        }

        try{

            Boolean provider_enabled    = (Boolean) extras.get(LocationManager.KEY_PROVIDER_ENABLED);
            Integer status_changed      = (Integer) extras.get(LocationManager.KEY_STATUS_CHANGED);
            Boolean proximity_entering  = (Boolean) extras.get(LocationManager.KEY_PROXIMITY_ENTERING);
            Log.e(TAG,"provider enabled: "+ String.valueOf(provider_enabled));
            Log.e(TAG,"status changed: "+ String.valueOf(status_changed));
            Log.e(TAG,"proximity entering: "+ String.valueOf(proximity_entering));
        }catch(Exception e){
            Log.e(TAG, LoginActivity.getStackTrace(e));
        }
    }
}