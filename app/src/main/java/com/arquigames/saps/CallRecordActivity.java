package com.arquigames.saps;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CallRecordActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private ArrayList<String> list;
    private StableArrayAdapter adapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_record);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        swipe = findViewById(R.id.swipe_call_records);
        swipe_list = findViewById(R.id.swipe_call_records_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        handler.post(new Runnable() {
            @Override
            public void run() {
                CallRecordActivity.this.getData();
                swipe.setRefreshing(false);
            }
        });

        /*
        ListAdapter listAdapter = new SwipeListAdapter();
        swipe_list.setAdapter(listAdapter);
        */
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        CallRecordActivity.this.getData();
                        swipe.setRefreshing(false);
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);

        AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = list.get(position);
                String file_url = CallRecordActivity.this.getRecordDirectory()+"/"+name;
                Intent playAudioIntent = new Intent(Intent.ACTION_VIEW);
                playAudioIntent.setDataAndType(Uri.parse(file_url), "audio/*");
                //CallRecordActivity.this.startActivity(playAudioIntent);

                Intent chooser = Intent.createChooser(playAudioIntent, "Reproducir");
                if (playAudioIntent.resolveActivity(getPackageManager()) != null) {
                    CallRecordActivity.this.startActivity(chooser);
                }

            }
        };
        swipe_list.setOnItemClickListener(onItemClickListener);
    }
    private String getRecordDirectory(){
        return Environment.getExternalStorageDirectory()+ "/"+ AppActivity.CALL_RECORD_DIRECTORY;
    }
    private void getData(){
        String[] values = new String[] { "Sin registros" };
        File sampleDir = new File(getRecordDirectory());
        if(sampleDir.isDirectory()){
            File[] files = sampleDir.listFiles();
            if(files!=null && files.length>0){
                values = new String[files.length];
                for(int i = 0 ; i < files.length;i++){
                    File file = files[i];
                    values[i] = file.getName();
                }
            }
        }
        fillList(values);
    }
    public void fillList(String[] values){

        list = new ArrayList<String>(Arrays.asList(values));
        adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        swipe_list.setAdapter(adapter);
    }
    public static class CallRecordFile{
        public final String name;
        public CallRecordFile(String name){
            this.name = name;
        }
    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
