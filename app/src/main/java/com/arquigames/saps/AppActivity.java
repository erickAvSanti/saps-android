package com.arquigames.saps;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arquigames.saps.fragments.FragmentCotizaciones;
import com.arquigames.saps.fragments.FragmentPolizas;
import com.arquigames.saps.services.RecordService;
import com.arquigames.saps.zonas.DepartamentoActivity;
import com.arquigames.saps.zonas.ItemDepartamentoListActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AppActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,LocationListener {

    private static final int REQUEST_CODE = 0x0002;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0x00001;
    private static final int MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION = 0x00002;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0x00003;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 0x00004;
    private static final int MY_PERMISSIONS_REQUEST_CAPTURE_AUDIO_OUTPUT = 0x00005;

    public static final String HOST_WS = "http://appws.ugaz.pe";
    public static final String URL_LOGOUT = HOST_WS+"/logout.php";
    public static final String URL_LOGIN = HOST_WS+"/login.php";
    public static final String URL_DEPARTAMENTOS = HOST_WS+"/zonas_departamentos_list.php";
    public static final String CALL_RECORD_DIRECTORY = "SAPS GRABADOR DE LLAMADAS";
    private JSONObject json;
    public static final String TAG = "SAPS_AppActivity";
    private View mProgressView;
    private View mListView;

    @Override
    public void onDestroy() {
        AppActivity.disableLocationService(this);
        super.onDestroy();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
        */

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        json = LoginActivity.getUserInfo(this);
        if(json==null){
            Intent intent = new Intent(AppActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }else{
            try {
                TextView textView = headerView.findViewById(R.id.navHeaderNameView);
                textView.setText(json.getString("nombres"));
                textView = headerView.findViewById(R.id.navHeaderEmailView);
                textView.setText(json.getString("email"));
            } catch (JSONException e) {
                Log.e(TAG,LoginActivity.getStackTrace(e));
            }
        }

        this.checkPhoneStatePermission();
        this.checkLocationPermission();
        this.checkWriteExternalStoragePermission();
        this.checkRecordAudioPermission();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            this.checkCaptureAudioOutputPermission();
        }


        /*
        intent = new Intent(this, RecordService.class);
        if (!isServiceRunning(RecordService.class)) {
            startService(intent);
        }
        */
        AppActivity.enableLocationService(this);

        mProgressView = findViewById(R.id.progress_bar_app);
        mListView = findViewById(R.id.content_app_list);

    }
    private boolean isServiceRunning(Class serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.e (TAG, "Service Running");
                return true;
            }
        }
        Log.e (TAG, "Service NOT Running");
        return false;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE == requestCode) {
            Intent intent = new Intent(this, RecordService.class);
            startService(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.showInfo();
            return true;
        }
        if (id == R.id.action_logout) {

            LoginActivity.logout(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_cotizaciones) {
            // Handle the camera action
            this.showNoticeCotizaciones();
        } else if (id == R.id.nav_clientes) {

        } else if (id == R.id.nav_polizas) {
            this.showNoticePolizas();

        } else if (id == R.id.nav_cobranzas) {

        } else if (id == R.id.nav_call_records) {
            this.showCallRecords();
        }else{

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
    public void showNoticeCotizaciones() {
        // Create an instance of the dialog fragment and show it
        FragmentCotizaciones dialog = new FragmentCotizaciones();
        dialog.show(getSupportFragmentManager(), "FragmentCotizaciones");
    }

    public void showNoticePolizas() {
        // Create an instance of the dialog fragment and show it
        FragmentPolizas dialog = new FragmentPolizas();
        dialog.show(getSupportFragmentManager(), "FragmentPolizas");
    }
    public void showCallRecords() {
        Intent intent = new Intent(this,CallRecordActivity.class);
        startActivity(intent);
    }

    private void showInfo(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("App: SAPS\nDesarrollador: Erick Avalos")
                .setTitle("SAPS v1.0");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getPhoneInfo() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String phone_imei;
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                phone_imei = tm.getImei();
            }else{
                phone_imei = tm.getDeviceId();
            }
        }
    }
    public static boolean hasPhoneStatePermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasLocationPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasWriteExternalStoragePermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
    }
    public static boolean hasRecordAudioPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean hasCaptureAudioOutputPermission(Context ctx){
        return ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CAPTURE_AUDIO_OUTPUT)== PackageManager.PERMISSION_GRANTED;
    }
    private void checkPhoneStatePermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
        }

    }
    private void checkLocationPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION);
            }
        }

    }
    private void checkWriteExternalStoragePermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }

    }
    private void checkRecordAudioPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
            }
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void checkCaptureAudioOutputPermission(){
        boolean granted;
        granted = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAPTURE_AUDIO_OUTPUT)== PackageManager.PERMISSION_GRANTED;
        if(!granted){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAPTURE_AUDIO_OUTPUT)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAPTURE_AUDIO_OUTPUT},
                        MY_PERMISSIONS_REQUEST_CAPTURE_AUDIO_OUTPUT);
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public static void enableLocationService(Context ctx) {
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1500, 3, (LocationListener) ctx);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1500, 3, (LocationListener) ctx);
    }
    public static void disableLocationService(Context ctx){
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        locationManager.removeUpdates((LocationListener) ctx);
    }

    public void onLocationChanged(Location location) {
        // Called when a new location is found by the network location provider.
        Log.e(TAG,"onLocationChanged, location"+location.toString());
        HashMap<String, String> hash = new HashMap<>();
        hash.put("loc_latitude", String.valueOf(location.getLatitude()));
        hash.put("loc_longitude", String.valueOf(location.getLongitude()));
        LoginActivity.putSharedStringArrayValues(this,hash);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG,"onStatusChanged, provider"+provider+", status: "+status+", "+extras.toString());
    }

    public void onProviderEnabled(String provider) {
        Log.e(TAG,"onProviderEnabled, provider"+provider);
    }

    public void onProviderDisabled(String provider) {
        Log.e(TAG,"onProviderDisabled, provider"+provider);
    }

    public void openDepartamentos(View view) {

        String source = LoginActivity.getSharedStringValue(this, DepartamentoActivity.KEY_SHARED_PREFERENCE);
        if(source!=null && source!=""){
            Intent intent = new Intent(AppActivity.this, DepartamentoActivity.class);
            intent.putExtra("result",source);
            startActivity(intent);
        }else{


            showProgress(true);

            RequestQueue queue = Volley.newRequestQueue(this);

            StringRequest objRequest = new StringRequest(Request.Method.POST, URL_DEPARTAMENTOS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    showProgress(false);
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response);
                        Boolean success= (Boolean)json.get("success");
                        if(success){
                            Log.e(TAG,response);

                            Intent intent = new Intent(AppActivity.this, DepartamentoActivity.class);
                            intent.putExtra("result",response);
                            startActivity(intent);

                        }else{
                            //TODO
                            Log.e(TAG, String.format("cant get info => %s", URL_DEPARTAMENTOS));
                        }
                    } catch (JSONException e) {
                        Log.e(TAG,LoginActivity.getStackTrace(e));
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    showProgress(false);
                    Log.e(TAG,error.toString());
                }
            });
            queue.add(objRequest);
        }

    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
            mListView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mListView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mListView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
