package com.arquigames.saps.zonas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.arquigames.saps.AppActivity;
import com.arquigames.saps.LoginActivity;
import com.arquigames.saps.R;
import com.arquigames.saps.webservices.WS_Departamentos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DepartamentoActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private ArrayList<String> list;
    private HashMap<String,JSONObject> list_map;
    private StableArrayAdapter adapter;
    private ListView swipe_list;
    private SwipeRefreshLayout swipe;
    public static String TAG = "DepartamentoActivity";
    public static String KEY_SHARED_PREFERENCE  ="departamentos_result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departamento);

        Intent intent_extras = this.getIntent();
        String source = "";
        if(intent_extras!=null){
            source = intent_extras.getStringExtra("result");
            if(source!="" && source!=null){
                LoginActivity.putSharedStringValue(this,KEY_SHARED_PREFERENCE,source);
            }else{
                source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
            }
        }else{
            source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
        }

        JSONArray arr = new JSONArray();
        try {
            if(source!=""){
                Log.e(TAG,"SOURCE => "+source);
                JSONObject json = new JSONObject(source);
                arr = json.getJSONArray("data");
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        swipe = findViewById(R.id.swipe_departamento);
        swipe_list = findViewById(R.id.swipe_departamento_list);

        swipe.setColorSchemeResources(R.color.color_swipe_list_foreground);
        swipe.setProgressBackgroundColorSchemeResource(R.color.color_swipe_list_background);

        if(source=="" || source==null){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    DepartamentoActivity.this.getData();
                }
            });
        }

        /*
        ListAdapter listAdapter = new SwipeListAdapter();
        swipe_list.setAdapter(listAdapter);
        */
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        DepartamentoActivity.this.getData();
                    }
                });
            }
        };
        swipe.setOnRefreshListener(onRefreshListener);

        AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JSONObject obj = list_map.get(((TextView)view).getText());
            }
        };
        swipe_list.setOnItemClickListener(onItemClickListener);
        this.fillList(arr);
    }
    public void fillList(JSONArray arr){
        String[] values;
        if(list_map!=null){
            list_map.clear();
        }else{
            list_map = new HashMap<String,JSONObject>();
        }
        if(arr.length()>0){
            values = new String[arr.length()];
            for(int i=0;i<arr.length();i++){
                try {
                    JSONObject obj = arr.getJSONObject(i);
                    String nombre = obj.getString("nombre");
                    values[i] = (i+1)+". "+nombre;
                    list_map.put(values[i],obj);
                } catch (JSONException e) {
                }
            }
        }else{
            values = new String[]{"Sin datos"};
        }
        list = new ArrayList<String>(Arrays.asList(values));
        adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        swipe_list.setAdapter(adapter);
    }
    private void getData(){
        WS_Departamentos.reload2(DepartamentoActivity.this);
        swipe.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }
    public static class CallRecordFile{
        public final String name;
        public CallRecordFile(String name){
            this.name = name;
        }
    }
    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
