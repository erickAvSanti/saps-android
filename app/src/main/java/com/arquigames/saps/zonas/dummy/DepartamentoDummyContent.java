package com.arquigames.saps.zonas.dummy;

import com.arquigames.saps.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DepartamentoDummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static int counter = 1;

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();
    public static void createFromJson(JSONArray source){
        counter = 1;
        ITEMS.clear();
        ITEM_MAP.clear();
        int size = source.length();
        if(size>0){
            for (int i = 0 ; i < size ; i++) {
                try {
                    JSONObject obj = source.getJSONObject(i);
                    String id = obj.getString("id");
                    String nombre = obj.getString("nombre");
                    String fecha_creacion_registro = obj.getString("fecha_creacion_registro");
                    DummyItem dum = new DummyItem(id,nombre,fecha_creacion_registro);
                    ITEMS.add(dum);
                    ITEM_MAP.put(id,dum);
                } catch (JSONException e) {
                    LoginActivity.getStackTrace(e);
                }
            }
        }else{
            DummyItem dum = new DummyItem("1","None","0000-00-00");
            ITEMS.add(dum);
            ITEM_MAP.put("1",dum);
        }
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String name;
        public final String created_at;
        public final int index;

        public DummyItem(String id, String name, String created_at) {
            this.index = DepartamentoDummyContent.counter++;
            this.id = id;
            this.name = name;
            this.created_at = created_at;
        }

        @Override
        public String toString() {
            return this.id+", "+this.name+", "+this.created_at;
        }
    }
}
