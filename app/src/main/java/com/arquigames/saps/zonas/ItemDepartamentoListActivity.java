package com.arquigames.saps.zonas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arquigames.saps.AppActivity;
import com.arquigames.saps.LoginActivity;
import com.arquigames.saps.R;

import com.arquigames.saps.webservices.WS_Departamentos;
import com.arquigames.saps.zonas.dummy.DepartamentoDummyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * An activity representing a list of ItemDepartamentos. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDepartamentoDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemDepartamentoListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    public static String TAG = "ItemDepartamentoListActivity";
    public static String KEY_SHARED_PREFERENCE  ="departamentos_result";

    private SimpleItemRecyclerViewAdapter adapter;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemdepartamento_list);

        Intent intent_extras = this.getIntent();
        String source = "";
        if(intent_extras!=null){
            source = intent_extras.getStringExtra("result");
            if(source!="" && source!=null){
                LoginActivity.putSharedStringValue(this,KEY_SHARED_PREFERENCE,source);
            }else{
                source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
            }
        }else{
            source = LoginActivity.getSharedStringValue(this,KEY_SHARED_PREFERENCE);
        }

        JSONArray arr = new JSONArray();
        try {
            if(source!=""){
                Log.e(TAG,"SOURCE => "+source);
                JSONObject json = new JSONObject(source);
                arr = json.getJSONArray("data");
            }
        } catch (JSONException e) {
            Log.e(TAG,LoginActivity.getStackTrace(e));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null){
            setSupportActionBar(toolbar);
            toolbar.setTitle(getTitle());
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "add item", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        FloatingActionButton fab_reload = (FloatingActionButton) findViewById(R.id.fab_reload);
        fab_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter!=null){
                    WS_Departamentos.reload(ItemDepartamentoListActivity.this);
                }
            }
        });

        if (findViewById(R.id.itemdepartamento_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        recyclerView = findViewById(R.id.itemdepartamento_list);
        assert recyclerView != null;
        this.fillList(arr);
    }
    public void fillList(JSONArray arr){
        DepartamentoDummyContent.createFromJson(arr);
        setupRecyclerView(recyclerView);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        adapter = new SimpleItemRecyclerViewAdapter(this, DepartamentoDummyContent.ITEMS, mTwoPane);
        recyclerView.setAdapter(adapter);
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemDepartamentoListActivity mParentActivity;
        private final List<DepartamentoDummyContent.DummyItem> mValues;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DepartamentoDummyContent.DummyItem item = (DepartamentoDummyContent.DummyItem) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDepartamentoDetailFragment.ARG_ITEM_ID, item.id);
                    ItemDepartamentoDetailFragment fragment = new ItemDepartamentoDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.itemdepartamento_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDepartamentoDetailActivity.class);
                    intent.putExtra(ItemDepartamentoDetailFragment.ARG_ITEM_ID, item.id);

                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(ItemDepartamentoListActivity parent,
                                      List<DepartamentoDummyContent.DummyItem> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.itemdepartamento_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            DepartamentoDummyContent.DummyItem dum = mValues.get(position);
            holder.mContent1View.setText(dum.index+".- "+dum.name);
            holder.mContent2View.setText(dum.created_at);

            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mContent1View;
            final TextView mContent2View;

            ViewHolder(View view) {
                super(view);
                mContent1View = (TextView) view.findViewById(R.id.id_content1);
                mContent2View = (TextView) view.findViewById(R.id.id_content2);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, AppActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
